#include "languages.h"

map<string, map<string, string>* >* language_map;
string current_language = default_language;
map<string, string>* read_language_file(string lang_code);

void change_language(string lang_code) {
    if(language_map == NULL) {
        language_map
            = new map<string, map<string, string>* >();
    }

    if(language_map->find(lang_code) == language_map->end()) {
        (*language_map)[lang_code]
            = read_language_file(lang_code);
    }
    current_language = lang_code;
}

string translate(string label) {
    return NULL;
}

 map<string, string>* read_language_file(string lang_code)
 {
    map<string, string>* lang = new map<string,string>();
    string full_name = lang_code + string(".txt");
    string line;
    ifstream f(full_name.c_str());
    while(!f.eof()) {
        getline(f, line);
        int index = line.find("=");
        if(index != string::npos) {
            string key = line.substr(0, index);
            string value = line.substr(index+1);
            (*lang)[key] = value;
        }
    }
    f.close();
    return lang;
 }

