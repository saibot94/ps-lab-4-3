#include "projectlibs.h"
#include "menus.h"
#include "languages.h"

using namespace std;

const string menu_names[] = {
  "New Game",
  "Options",
  "Quit"
};

const string new_game_submenu_names[] = {
    "Choose difficulty",
    "Start",
    "Back"
};

const string options_submenu_names[] = {
    "Change language",
    "Graphics",
    "Back"
};

/* TODO

Use this function by passing an array and its length. A new submenu vector
will come out
*/
vector<MenuItem*>* menu_from_array(MenuItem** arr, unsigned int len) {
    if(len > 0) {
        unsigned int sz = sizeof(arr) * len;
        return new vector<MenuItem*>(arr, arr + sz / sizeof(arr[0]));
    } else {
        return new vector<MenuItem*>();
    }
}


/*
TODO:

Return a menu with the following structure:
- Main Menu
    - New Game
        - Choose difficulty
        - Start
        - Back
    - Options
        - Change language
        - Graphics
        - Back
    - Quit

Tbe back button should show you the previous menu.
The quit menu should exit the program entirely. Hint: you can use the exit(int) function to quit a program.
All other action buttons will just print a message then return to the caller menu.
*/
MenuItem* build_menu()
{
    MenuItem* new_game_submenu[] =  {};
    MenuItem* options_submenu[] =  {};
    MenuItem*  main_menu[] = {
        new SubmenuItem(menu_names[0], *menu_from_array(new_game_submenu, 0)),
        new SubmenuItem(menu_names[1], *menu_from_array(options_submenu, 0))
    };
    return new SubmenuItem("Main Menu", *menu_from_array(main_menu, 2));
}








int main()
{
    change_language(default_language);

    MenuItem* option1 = new ActionMenuItem("CHANGE_LANG_LABEL");
    MenuItem* option2 = new ActionMenuItem("GRAPHICS_LABEL");
    MenuItem* option3 = new BackMenuItem("BACK_LABEL");
    vector<MenuItem*>* options = new vector<MenuItem*>();
    options->push_back(option1);
    options->push_back(option2);
    options->push_back(option3);
    MenuItem* action1 = new ActionMenuItem("NEW_GAME_LABEL");
    MenuItem* action2 = new SubmenuItem("OPTIONS_LABEL", *options);
    MenuItem* action3 = new BackMenuItem("QUIT_LABEL");
    vector<MenuItem*>* submenus = new vector<MenuItem*>();
    submenus->push_back(action1);
    submenus->push_back(action2);
    submenus->push_back(action3);

    MenuItem* main_menu = new SubmenuItem("MAIN_MENU_LABEL",
                                          *submenus);

    main_menu->do_stuff();
    return 0;
}
