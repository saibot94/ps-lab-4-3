#pragma once
#include "projectlibs.h"

const string default_language = "en";

void change_language(string lang_code);
string translate(string label);
