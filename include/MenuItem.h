#ifndef MENUITEM_H
#define MENUITEM_H
#include "projectlibs.h"


class MenuItem
{
    private:
        string name;
    public:
        MenuItem(string _name): name(_name) {}
        virtual bool do_stuff() = 0;
        string get_name(){ return name; }
};


#endif // MENUITEM_H

